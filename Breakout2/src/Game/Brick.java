package Game;

import acm.graphics.*; 
import acm.util.*;  
import java.awt.*; 

public class Brick extends GCompound {

	public Brick(int colNum, int rowNum, Color color) {
	
	    if (rgen.nextBoolean(0.01)) {										//Eriline telliskivi, annab erilised omadused
	        color = Color.MAGENTA;
	        special = true;
	    }
	    GRect fill = new GRect(0,0,BRICK_WIDTH, BRICK_HEIGHT);
	    fill.setFilled(true);
	    fill.setColor(color);
	    GRect light = new GRect(0,0,BRICK_WIDTH,BRICK_HEIGHT);
	    light.setFilled(true);
	    light.setColor(new Color(255,255,255,120+10*rowNum));
	    if (special == true) light.setColor(new Color(255,255,255,120));
	
	
	
	    add(fill);
	    add(light);
	
	    double x = START_X + colNum * (BRICK_WIDTH + BRICK_SEP);
	    double y = START_Y + rowNum * (BRICK_HEIGHT + BRICK_SEP);
	    setLocation(x,y);
	
	    // Add to bricks remaining;
	    bricksRemaining++;
	}
	
	public static int getBricksRemaining() {
	    return bricksRemaining;
	}
	
	/* vähendab telliseid ja kontrollib kui palju neid alles on */
	public static boolean decreaseBricks() {
	    bricksRemaining--;
	    if (bricksRemaining < 75) Ball.setVelocity(7); //suurendab kiirust iga teatud telliste arvu kohal
	    if (bricksRemaining < 50) Ball.setVelocity(9); 
	    if (bricksRemaining < 25) Ball.setVelocity(11); 
	    return (bricksRemaining > 0) ? false : true;
	}
	
	public boolean isSpecial() {
	    return special;
	}
	
	 /** Separation between bricks */ 
	    public static final int BRICK_SEP = 4;
	
	 /** Width of a brick */ 
	    public static final int BRICK_WIDTH = 
	    (Breakout.WIDTH - (Breakout.NBRICKS_PER_ROW - 1) * BRICK_SEP) / Breakout.NBRICKS_PER_ROW; 
	
	 /** Height of a brick */ 
	    public static final int BRICK_HEIGHT = 12;
	
	
	/** Starting point X and Y values for first brick */
	    private static final double START_X = (Breakout.WIDTH - ((Breakout.NBRICKS_PER_ROW * BRICK_WIDTH) + (Breakout.NBRICKS_PER_ROW - 1) * BRICK_SEP)) / 2 ;      
	    private static final int START_Y = Breakout.BRICK_Y_OFFSET;
	
	
	    private boolean special = false;
	
	    private static int bricksRemaining = 0;
	
	    private RandomGenerator rgen = RandomGenerator.getInstance();
}