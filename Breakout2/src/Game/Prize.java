package Game;

import acm.graphics.*; 
import java.awt.*; 


public class Prize extends GOval {


	public Prize(double x, double y, Color color, Paddle pad) {														
	    super(x + Brick.BRICK_HEIGHT/2, y + Brick.BRICK_WIDTH/2, PRIZE_RADIUS * 2, PRIZE_RADIUS * 2);
	    setFilled(true);
	    setColor(color);
	
	    paddle = pad;
	}
	
	public void drop() {
	    this.move(0, PRIZE_SPEED);
	    paddle.checkForPrize(this,this.getX()+PRIZE_RADIUS,this.getY()+PRIZE_RADIUS*2);
	}
	
	public boolean leftScreen() {
	    if (this.getY() - PRIZE_RADIUS > Breakout.HEIGHT || pickedUp == true) {
	        return true;
	    } else {
	        return false;
	    }   
	}
	
	public void pickUp() {
	    pickedUp = true;
	}
	
	private boolean pickedUp = false;
	
	private Paddle paddle;
	
	public static final int PRIZE_RADIUS = 5;
	public static final int PRIZE_SPEED = 10;
}