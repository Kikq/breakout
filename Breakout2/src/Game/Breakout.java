package Game;
//TÄPITÄHED
/*Koodi võtsin http://codereview.stackexchange.com/questions/7865/building-a-breakout-game-from-scratch
*/
import acm.graphics.*; 
import acm.program.*; 
import java.awt.*;
import javax.sound.sampled.*;
import java.awt.event.*;
import java.io.File;

public class Breakout extends GraphicsProgram implements KeyListener, ComponentListener{ 
	
	static Clip clip = null;
	static DataLine.Info info = null;
	static AudioFormat aform = null;
	static AudioInputStream ais = null;
	static File url = null;
	public static int FPS;
	private static long start = 0;
	public static int levelTime = 999;
	
	Dimension dim = new Dimension();
	
	
	/* STanfordi kursuse ülesande lehelt mahakirjutatud alused, millest lähtudes hakati mängu kirjutama */

	public static final int APPLICATION_WIDTH = 400; 
	public static final int APPLICATION_HEIGHT = 600;
	public static final int WIDTH = APPLICATION_WIDTH; 
	public static final int HEIGHT = APPLICATION_HEIGHT;
	public static final int PADDLE_Y_OFFSET = 30; 
	public static final int NBRICKS_PER_ROW = 10;
	private static final int NBRICK_ROWS = 10;
	private static final int BALL_RADIUS = 10;
	public static final int BRICK_Y_OFFSET = 70;
	private static final int NTURNS = 3;
	private int score = 0;
	private int lives = NTURNS;  
	private boolean gameOver = false;  
	private GLabel lifeBar = new GLabel("Lives: " + lives);
	private GLabel scoreBar = new GLabel("Score: " + score);
	private GLabel timerBar = new GLabel ("Timer: " + levelTime);
	
	Prize prize = null;
	private Paddle paddle = new Paddle();       
	private Ball ball = new Ball(BALL_RADIUS);

	
	/**
	 * Käivitab appleti akna, laeb kõik antud elemendid, et mäng oleks valmis tegudeks
	 */
	public void init() { 	
		dim.setSize(WIDTH, HEIGHT);
		setSize(dim);
	    setBricks();
	    setBackground(Color.BLACK);
	    add(paddle);
	    add(ball,WIDTH/2,500);
	    add(lifeBar,20,60);
	    add(scoreBar,80, 60);
	    add(timerBar, 150, 60);
	    addMouseListeners();
	    addKeyListeners();
	    lifeBar.setColor(Color.WHITE);
	    scoreBar.setColor(Color.WHITE);
	    timerBar.setColor(Color.WHITE);
	    loadResources();
	    addComponentListener(this);

	}
	
	/**
	 * Alustab mänguga, paneb palli 2.sek pärast liikuma
	 */
	public void run() { 										
		while(gameOver != true) {
	        pause(20);
	        checkCollision(ball);
	        ball.move();
	        timer();

	        if (prize != null) {
	            prize.drop();
	            if (prize.leftScreen()) remove(prize);
	        }
		}
	} 
	
	/**
	 * Asetab tellised, annab õigele reale õige värvi
	 */
	private void setBricks() {									
	    for (int row = 0; row < NBRICK_ROWS; row++) {
	        Color rowColor = checkColor(row);   
	        for (int col = 0; col < NBRICKS_PER_ROW; col++) {
	            add(new Brick(col,row,rowColor));
	        }
	    }
	}
	/**
	 * Testimise jaoks!! Vajutades kaotab tellise
	 */
	public void mouseClicked(MouseEvent e) {
		removeBrick(getElementAt(e.getX(),e.getY()));
	}
	
	/**
	 * Reket liigub hiire järgi
	 */
	public void mouseMoved(MouseEvent e) {							
		paddle.followMouse(e.getX());
	}
	/**
	 * ********************************************************************
	 * KeyPressed - kui nuppu all hoiad, siis liigub õiges suunas.
	 */
	public void keyPressed(KeyEvent ke) {
		int keycode = ke.getKeyCode();				//returns integer to whatever key was recently pressed
		if(keycode == KeyEvent.VK_LEFT){
			paddle.setLeft(true);
		}
		else if(keycode == 39){
			paddle.setRight(true);
		}

	}
	/**
	 * *********************************************************************
	 * KeyReleased - Kui allhoitud nupust lahti lased, lõpetab liikumise.
	 */

	public void keyReleased(KeyEvent ke) {
		int keycode = ke.getKeyCode();	
		if(keycode == KeyEvent.VK_LEFT){
			paddle.setLeft(false);
		}
		else if(keycode == 39){
			paddle.setRight(false);
		}
	}
	/**
	 * Annab tellistele värvid
	 * @param row - rea numbri alusel annab värvi
	 * @return muul juhul tagastab algse värvi (sinaka)
	 */
	private Color checkColor(int row) {						
	    switch (row) {
	        case 0:
	        case 1: return Color.RED;
	        case 2:
	        case 3: return Color.ORANGE;
	        case 4:
	        case 5: return Color.YELLOW;
	        case 6:
	        case 7: return Color.GREEN;
	        case 8:
	        case 9: return Color.CYAN;
	    }
	    return Color.CYAN;
	}

	/**
	 * Palliga kokkupuutel kaotab tellise
	 * @param brick 
	 */
	private void removeBrick(GObject brick) {					
	    if (brick != null && brick instanceof Brick) {
	        remove(brick);
	        if (((Brick) brick).isSpecial()) callPrize(brick.getX(),brick.getY());
	        if (Brick.decreaseBricks()) callGameOver(true);
	        addScore();
	    }
	}
	//***************************************************************
	private void timer(){
		//See if lause kontrollib kas on möödunud 1000ms.
		if(System.currentTimeMillis()- start >=1000){
			levelTime --;
			start = System.currentTimeMillis();
			changeTime();
			
		}
	}private void changeTime(){
		if(levelTime != 0){
			timerBar.setLabel("Time: "+ levelTime);
		}else {callGameOver(false);}
	}
	
	private void addScore(){										//loeb skoori
		score++;
		scoreBar.setLabel("Score: " + score);
		//************************************************************
	}

	private void removeLife() {										//Kaotab elu
	    lives--;
	    lifeBar.setLabel("Lives: " + lives);
	    if (lives <= 0) callGameOver(false);
	}

	public void callPrize(double x, double y) {						//Kui tellis on MAGNETA-värvi, kukutab 'Powerup'-i
	    prize = new Prize(x,y,Color.MAGENTA, paddle);
	    add(prize);
	    addScore();
	}

	public void callGameOver(boolean won) {							//Mäng läbi

	    GRect gameOverScreen = new GRect(0,0,WIDTH,HEIGHT);
	    gameOverScreen.setFilled(true);
	    gameOverScreen.sendToFront();
	    GLabel gameOverLabel = new GLabel("");
	    if (won == true) {
	        gameOverScreen.setColor(Color.WHITE);
	        gameOverLabel.setColor(Color.BLUE);
	        gameOverLabel.setLabel("Jesss!!!  " + "\n Sinu skoor: " + score);
	        if (lives == 3) gameOverLabel.setLabel("Meganice, max score!!!  " +  "\r\n Sinu skoor: " + score);
	    } else {
	        gameOverScreen.setColor(Color.RED);
	        gameOverLabel.setColor(Color.WHITE);
	        gameOverLabel.setLabel("ega siis midagi, otsast peale   "+ "\r\n Sinu skoor: " + score);
	    }
	    add(gameOverScreen);
	    add(gameOverLabel,WIDTH/2-gameOverLabel.getWidth()/2,HEIGHT/2+gameOverLabel.getAscent()/2);
	
	    gameOver = true;
	}


	private void checkCollision(Ball ball) {							//Palli põrkumine
	        double r = ball.getRadius();
	        ball.move();
	        double x = ball.getX();
	        double y = ball.getY(); 
	        ball.moveBack();
	        GObject collider = null;
	        
	        int dir = 0;
	    
	        
	        		

	        if (x-r <= 0) {
	            ball.setLocation(r,y);
	            ball.changeDirection(1);
	        } else if (x+r >= WIDTH) {
	            ball.setLocation(WIDTH-r,y);
	            ball.changeDirection(1);
	        }
	
	        if (y-r <= 0) {
	            ball.setLocation(x,r);
	            ball.changeDirection(2);

	        } else if (y+r >= HEIGHT) {									//Võtab elu maha, kui palli kaotad
	            removeLife();
	            ball.restart();
	        }
	        //Parem külg
	        if (getElementAt(x+r,y) != null && getElementAt(x+r,y) != ball && !(getElementAt(x+r,y) instanceof GLabel) && !(getElementAt(x+r,y) instanceof Prize)) {
	            ball.changeDirection(1);
	            collider = getElementAt(x+r,y);
	           
	        } 
	        //Vasak külg
	        if (getElementAt(x-r,y) != null && getElementAt(x-r,y) != ball && !(getElementAt(x-r,y) instanceof GLabel) && !(getElementAt(x-r,y) instanceof Prize)) {
	            ball.changeDirection(1);
	            collider = getElementAt(x-r,y);
	        } 
	        //ALumine äär
	        if (getElementAt(x,y+r) != null && getElementAt(x,y+r) != ball && !(getElementAt(x,y+r) instanceof GLabel) && !(getElementAt(x,y+r) instanceof Prize)) {
	            ball.changeDirection(2);
	            collider = getElementAt(x,y+r);
	        } 
	        //Ülemine äär
	        if (getElementAt(x,y-r) != null && getElementAt(x,y-r) != ball && !(getElementAt(x,y-r) instanceof GLabel) && !(getElementAt(x,y-r) instanceof Prize)) {
	            ball.changeDirection(2);
	            collider = getElementAt(x,y-r); 
	        } 
	
	        ball.changeDirection(dir);								//muudab palli suunda
	        if (collider instanceof Brick) {
	        	removeBrick(collider);
	        	playSound();
	        }
	        if (collider == paddle) {
	            ball.setDirection(paddle.collide(x,r));
	            playSound();
	        }
	    
	}
	//*****************************************************************''
	public static void loadResources (){
		url = new File("pong.wav");
		try{
			
			ais = AudioSystem.getAudioInputStream(url);
			aform = ais.getFormat();
			info = new DataLine.Info(Clip.class, aform);
			clip = (Clip) AudioSystem.getLine(info);
			clip.open(ais);
		}
		catch (Exception ex){
			System.out.println(ex + " loadres");
		}
	}
	
	public static void playSound() {
		clip.setMicrosecondPosition(0);
		try{
			clip.loop(0);
		}
		catch (Exception ex){
			System.out.println(ex + " playsound");
		}
			
	}
	
	public void componentResized(ComponentEvent e) {
		setSize(dim);
	}

		@Override
		public void componentHidden(ComponentEvent e) {
			// TODO Auto-generated method stub
		}

		@Override
		public void componentMoved(ComponentEvent e) {
			// TODO Auto-generated method stub
		}
		@Override
		public void componentShown(ComponentEvent e) {
			// TODO Auto-generated method stub
		}
 }