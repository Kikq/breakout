package Game;

import acm.graphics.*; 
import java.awt.*; 

public class Paddle extends GCompound {
	private boolean left=false,right=false;

	/**
	 * Joonistab reketi
	 */
	public Paddle() {																		
	    fill = new GRect(0,0, PADDLE_STARTING_WIDTH, PADDLE_HEIGHT);
	    fill.setFilled(true);
	    fill.setColor(Color.GRAY);
	    light = new GRect(0,PADDLE_HEIGHT/2, PADDLE_STARTING_WIDTH, PADDLE_HEIGHT/2);
	    light.setFilled(true);
	    light.setColor(new Color(0,0,0,50));
	    add(fill);
	    add(light);
	    this.setLocation((Breakout.WIDTH - PADDLE_STARTING_WIDTH) / 2, PADDLE_Y);
	}
	/**
	 * 
	 * @param mouseX 
	 */
	public void followMouse(double mouseX) {													
	    if (mouseX < paddleWidth/2) {
	        this.setLocation(0, PADDLE_Y);
	    } else if (mouseX > Breakout.WIDTH - paddleWidth/2) {
	        this.setLocation(Breakout.WIDTH - paddleWidth,PADDLE_Y);
	    } else {
	        this.move(mouseX-paddleWidth/2 - this.getX(), 0);
	    }
	}
	//***************************************************************************
	public void setLeft(boolean b){
		left = b;
		if(left){
			//kui x koord. on vähem kui 5, asetab PADDLE tagasi 0 koord.
			if(this.getX() < 5){
				this.setLocation(0, PADDLE_Y);
			}
			else{
				this.move(-10 , 0);
			}
		}
	}
	public void setRight(boolean b){
		right = b;
		if(right){
			if (this.getX() >= Breakout.WIDTH - paddleWidth){
				this.setLocation(Breakout.WIDTH - paddleWidth, PADDLE_Y);
			}
			else{
				this.move(10 ,  0);
			}
			
		}
	}
	
	public double collide(double ballX, double r) {												//reketi pealt põrkumine
	    double paddleX = this.getX();
	    setColor(Color.RED);
	    double hitSpot = ballX - paddleX;
	    double maxPaddle = paddleWidth + r;
	    double minPaddle = -r;
	    double paddleRange = maxPaddle - minPaddle;
	    double minAngle = 160;
	    double maxAngle = 20;
	    double angleRange = maxAngle - minAngle;
	    double newDirection = ((hitSpot * angleRange) / paddleRange) + minAngle;
	
	
	    return newDirection;
	}
	
	private void widen() {																		//Powerupi saades laieneb reket
	    paddleWidth += 30;
	    this.move(-15, 0);
	    fill.setSize(paddleWidth, PADDLE_HEIGHT);
	    light.setSize(paddleWidth, PADDLE_HEIGHT / 2);
	}
	
	public void checkForPrize(Prize prize, double x, double y) {								//Kontrollib, et kas reket püüdis powerupi kinni
	    if ((y >= PADDLE_Y) && (y <= PADDLE_Y + PADDLE_HEIGHT) && x > this.getX() && x < this.getX() + paddleWidth) {
	        prize.pickUp();
	        this.widen();
	    }
	}
	
	
	 /** Dimensions of the paddle */ 
	private static final int PADDLE_STARTING_WIDTH = 60; 
	private static final int PADDLE_HEIGHT = 15;
	
	private GRect fill;
	private GRect light;
	private int paddleWidth = PADDLE_STARTING_WIDTH;
	 /** Dimensions of the paddle */ 
	public static final int PADDLE_Y = Breakout.HEIGHT - Breakout.PADDLE_Y_OFFSET;
}
