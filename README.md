README
Breakout2 mäng - http://codereview.stackexchange.com/questions/7865/building-a-breakout-game-from-scratch (Breakout esimene on tehtud youtube-i tutoriali järgi (Youtuber: 19cupsofjava), aga tutorial jäi poolikuks)
What is this repository for?
Kuna ise olen veel väga algaja(ning ei oska aega üldse planeerida), siis pidin oma algse plaani kõrvale jätma ja proovima midagi muud. Võtsin foorumist kellegi koodi (viitasin ära), ja annan endast parima et proovida tema koodi täiendada/parandada.
NB! Menu.java on kõigest iluasi praegu, ei tööta (veel!)
Mida tahtsin teha:
Avamenüü - Avamenüü töötab, aga nuppu vajutades hakkab jonnima (ei tea millest)

Heli - DONE

Appleti menüü 'restart' nupp korda saada - Appleti enda menüüd ei saagi confida

Skoor - DONE

Timer - DONE

Anda mõnele tellisele erilisemad omadused

Nooleklahvidega juhitavuse -DONE (+border)

Parandada orginaalkoodi foormupostituste järgi - foorumipostitused kõigest soovitasid mingeid lihtsmaid loogikalahendusi, polnud mõtet hakata orginaalkoodi muutma.

Sain igasuguseid uusi kogemusi, kahjuks ei töötanud neist 95%, tahan veel sellel pühenduda ja oma vigadest aru saada.